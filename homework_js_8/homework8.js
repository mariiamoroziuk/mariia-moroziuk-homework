function onFocus() {
    inp.style.borderColor = 'green';
    inp.style.borderWidth = '10px';
}

function noFocus() {
    inp.style.borderColor = '';
    inp.style.borderWidth = '';

    if ((inp.value) < 0) {
        inp.style.borderColor = 'red';

        if(!document.getElementById('p')) {
            let p = document.createElement('p');
            p.id = "p";
            p.innerText = ('Please enter correct price');

            form.appendChild(p);
        }
    } else if (inp.value){
        if(!document.getElementById('span')) {

            let span = document.createElement('span');
            span.id = "span";
            inp.style.backgroundColor = 'green';
            span.innerHTML = "Текущая цена: $" + `${inp.value}`;

            form.insertBefore(span, inp);
        }

        if(!document.getElementById('reset')){

            let reset= document.createElement('input');
            reset.id = 'reset';
            reset.type = "reset";
            reset.value="X";
            form.insertBefore(reset, inp);

            reset.addEventListener("click", deleteSpanAndInputReset);
        }
    }
}
function deleteSpanAndInputReset() {

        inp.style.backgroundColor = '';
        inp.value = '';

        let span = document.getElementById('span');
        form.removeChild(span);

        let reset = document.getElementById('reset');
        form.removeChild(reset);
}
