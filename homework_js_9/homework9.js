const inputs=document.getElementById('tabs');

inputs.onclick = function(event) {
    let name = event.target.className;
    let classColection = document.getElementsByClassName(name);
    let activeText = document.getElementsByClassName("active_text")[0];

    if (!activeText) {
        classColection[1].classList.add('active_text');
    } else {
        activeText.classList.remove('active_text');
        classColection[1].classList.add('active_text');
    }

    let ev = event.target;
    let activeInput = document.getElementsByClassName("color")[0];

    if (!activeInput) {
        ev.classList.add('color');
    } else {
        activeInput.classList.remove('color');
        ev.classList.add('color');
    }
}