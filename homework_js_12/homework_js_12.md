
Если в функцию setTimeout() передать нулевую задержку, она сработает но с задержкой от 0 до 4мс, потому что по стандарту минимальная задержка составляет 4 мс

Функция setTimeout() откладывает исполнение функции(строки) на заданное время, а функция setInterval() запускает функцию(строку) регулярно через указанный интервал времени.

Вызывать функцию clearInterval(), когда цикл запуска уже не нужен необходимо, поскольку внутренняя ссылка исчезнет только при очистке таймера, а пока она не исчезла, она будет приводить к излишним тратам памяти
