function showPicturesInterval() {
        let img = document.getElementsByClassName('visible')[0];
        img.classList.remove('visible');

        if (img.nextElementSibling.className ==='image-to-show'){
            img.nextElementSibling.classList.add('visible');
        } else{
        img= document.getElementsByTagName('img')[0];
        img.classList.add('visible');
        }
    }
function onButtonClick(){
    clearInterval(timerId)}
function onSecondButtonClick(){
    timerId = setInterval(showPicturesInterval, 3000)}

const button = document.getElementsByTagName('button')[0];
const secondButton = document.getElementsByTagName('button')[1];
let timerId = setInterval(showPicturesInterval,3000);

button.addEventListener('click', onButtonClick);
secondButton.addEventListener('click', onSecondButtonClick);

showPicturesInterval();