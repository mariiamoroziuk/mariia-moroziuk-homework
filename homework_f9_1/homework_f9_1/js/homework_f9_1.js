function initMenu() {
    $('.fa-bars').click(
        function () {
            $('.fa-times').show();
            $('.header-menu').show();
            $(this).hide();
        });
    $('.fa-times').click(
        function () {
            $('.fa-bars').show();
            $('.header-menu').hide();
            $(this).hide();
        });
}
$(document).ready(function (){
    initMenu()
});

