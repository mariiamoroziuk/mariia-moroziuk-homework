document.addEventListener('DOMContentLoaded', onReady);
const button = document.getElementById('button');

function onReady() {
    button.onclick = async function () {
        const reqIP = await fetch('https://api.ipify.org/?format=json');
        const value = await reqIP.json();

        const ip = value.ip;
        const url = 'http://ip-api.com/json/';
        const fieldsAndLang = '?lang=ru&fields=continent,country,region,regionName,city';

        const reqAddress = await fetch(`${url + ip + fieldsAndLang}`);
        const data = await reqAddress.json();

        const ul = document.createElement('ul');
        ul.innerHTML = `<li>Continent: ${data.continent}</li><li>Country: ${data.country}</li><li>Region: ${data.region}</li><li>Region name:${data.regionName}</li><li>City: ${data.city}</li>`;
        document.body.append(ul);
    }

}