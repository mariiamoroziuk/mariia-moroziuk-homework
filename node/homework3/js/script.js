document.addEventListener('DOMContentLoaded', onReady);

const button=document.querySelector('.buttons .first button');

function onReady() {
    button.onclick = function () {
        document.cookie = "experiment=novalue; max-age=300";

        if (((document.cookie).split('; ')).some(function (item) {
           return  /^new-user/.test(item)
        })){
            document.cookie = "new-user=false"
        } else {
            document.cookie = "new-user=true"}
    }
}