document.addEventListener('DOMContentLoaded', onReady);

function onReady() {
    const req = new XMLHttpRequest();

    req.open('GET', `https://swapi.co/api/films/`);

    req.onload = function () {

        if (req.readyState === 4 && req.status === 200) {

            const value = JSON.parse(req.response);
            const ul = document.createElement('ul');
            document.body.appendChild(ul);

            value.results.forEach(function (obj) {
                const li = document.createElement('li');
                const id = obj.episode_id;
                const title = obj.title;
                const crawl = obj.opening_crawl;

                li.innerHTML = 'EPISODE' + ' ' + id + ' :' + title + ' (' + crawl + ')';
                ul.appendChild(li);

                const characters = obj.characters;
                const ulCharacters = document.createElement('ul');
                for (let i = 0; i < characters.length; i++) {
                    const charactRequest = new XMLHttpRequest();
                    charactRequest.open('GET', `${characters[i]}`);
                    charactRequest.onload = function () {
                        if (charactRequest.readyState === 4 && charactRequest.status === 200) {
                            const liCharacters = document.createElement('li');
                            liCharacters.innerHTML = (JSON.parse(charactRequest.response)).name;
                            ulCharacters.appendChild(liCharacters);
                        }
                    };
                    charactRequest.send();
                }
                li.appendChild(ulCharacters)
            })
        }
    };

    req.send();

}
