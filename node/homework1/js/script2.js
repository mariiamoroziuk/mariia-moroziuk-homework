document.addEventListener('DOMContentLoaded', onReady);

function status(response) {
    if (response.status === 200) {
        return Promise.resolve(response)
    } else {
        return Promise.reject(new Error(response.statusText))
    }
}

function json(response) {
    return response.json()
}

function onReady() {

    fetch(`https://swapi.co/api/films/`)
        .then(status)
        .then(json)
        .then(function (data) {
            const ul = document.createElement('ul');
            document.body.appendChild(ul);
            data.results.forEach(function (obj) {

                const li = document.createElement('li');
                const id = obj.episode_id;
                const title = obj.title;
                const crawl = obj.opening_crawl;

                li.innerHTML = 'EPISODE' + ' ' + id + ' :' + title + ' (' + crawl + ')';
                ul.appendChild(li);

                const characters = obj.characters;
                const ulCharacters = document.createElement('ul');
                const arrOfCharactersFetches = [];
                for (let i = 0; i < characters.length; i++) {
                    arrOfCharactersFetches.push(fetch(`${characters[i]}`)
                        .then(status)
                        .then(json)
                    )
                }
                Promise.all(arrOfCharactersFetches)
                    .then(function (data) {
                        data.forEach(function (object) {
                            const liCharacters= document.createElement('li');
                            liCharacters.innerHTML = object.name;
                            ulCharacters.appendChild(liCharacters);
                        })
                    });
                li.appendChild(ulCharacters);
            })
        }).catch(function (error) {
        console.log(error);
    });
}