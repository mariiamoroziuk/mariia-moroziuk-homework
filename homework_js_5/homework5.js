function createNewUser(name,secondName,date) {
    newUser = {
        firstName: name,
        lastName: secondName,
        birthday: date,
        makeLogin: function getLogin() {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
        makePassword: function getPassword() {
                return this.firstName.charAt(0).toUpperCase() +
                this.lastName.toLowerCase() +
                this.birthday.slice(6, 10)
        },
        makeAge: function getAge(){
             if ((new Date().getMonth()+1)>(+(this.birthday.slice(3, 5)))){
                 return(
                    new Date().getFullYear() - +(this.birthday.slice(6, 10)))
             }else if ((new Date().getMonth()+1)<(+(this.birthday.slice(3, 5)))){
                 return (
                     new Date().getFullYear() - +(this.birthday.slice(6, 10)) -1)
             } else if(new Date().getDate()>=(+(this.birthday.slice(0, 2)))) {
                 return (
                     new Date().getFullYear() - +(this.birthday.slice(6, 10))
                 )
             } else return (
                     new Date().getFullYear() - +(this.birthday.slice(6, 10)) -1)
             }
        };
    return newUser;
}


console.log(createNewUser(prompt('What is your name?'),
                          prompt ('What is your last name?'),
                          prompt ('what date of your birthday?', 'dd.mm.yyyy')),
            newUser.makeLogin(),
            newUser.makePassword(),
            newUser.makeAge());

