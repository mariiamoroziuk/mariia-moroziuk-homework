const button = document.getElementsByClassName('button')[0];
button.addEventListener('click', onButtonClick);
const cont = document.getElementsByClassName('cont')[0];

if(!(localStorage["class"])){
    cont.classList.add('one');
} else {cont.classList.add(localStorage["class"])}

function onButtonClick() {
    cont.classList.toggle('one');
    cont.classList.toggle('another');
    let x = cont.classList[1];
    if (x === 'one') {
        localStorage["class"] = "one"
    } else if (x === 'another'){
        localStorage["class"] = "another"
    }
}