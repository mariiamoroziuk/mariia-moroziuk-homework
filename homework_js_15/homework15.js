$(function(){
    $('body').append('<a href="#" id="go-top" title="up">UP</a>');
});

$(function() {
    $.fn.scrollUp = function() {
        $(this).hide().removeAttr("href");
        if ($(window).scrollTop() >= $(window).height()) {$(this).fadeIn()}
        const upButton = $(this);
        $(window).scroll(function() {
            if ($(window).scrollTop() <= $(window).height()) {$(upButton).fadeOut()}
            else {$(upButton).fadeIn()}
        });
        $(this).click(function() {
            $("html, body").animate({scrollTop: 0}, 1000)
        })
    }
});

$(function() {
    $("#go-top").scrollUp();
});

$(function(){
    $('.top_menu a').on('click', function(e){
        let a = $(this).attr('href');
        $('html,body').stop().animate({ scrollTop: $(`${a}`).offset().top }, 'slow');
        e.preventDefault();
    });
});

$(function () {
    $('#expand_roll').click(function(){
        $('.slide').slideToggle()
    })
});