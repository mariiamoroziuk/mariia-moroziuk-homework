class Hamburger {

    constructor(size, stuffing){
    this._size=size;
    this._staffing=stuffing;
    this.toppings=[];
    if (!this._size) {
        throw new HamburgerException("no size given");
    }
    if (!this._staffing) {
        throw new HamburgerException("no staffing given");
    }
    }

    addTopping(topping){
        if (this.toppings.some(function (item) {return item===topping})) {
            throw new HamburgerException(" duplicate topping ");
        }
        return this.toppings.push(topping);
    }

    removeTopping(topping) {
        if (this.toppings.some(function (item) {
            return item !== topping
        })) {
            throw new HamburgerException("this topping is not in order");
        }
        this.toppings = this.toppings.filter(function (item) {
            return item !== topping
        })
    }

    getTopping(){
        return this.toppings.length>0? this.toppings: 'in order not toppings'
    }

    getSize(){
        return this._size
    }

    getStuffing(){
        return this._staffing
    }

    calculatePrice(){
        let price = 0,
            hamburgerSizeSmallPrice=50,
            hamburgerSizeLargePrice=100,
            hamburgerStaffingCheesePrice=10,
            hamburgerStaffingSaladPrice=20,
            hamburgerStaffingPotatoPrice=15,
            hamburgerToppingMayoPrice=20,
            hamburgerToppingSpicePrice=15;

        if(this._size===Hamburger.SIZE_SMALL){
            price+=hamburgerSizeSmallPrice
        }
        if(this._size===Hamburger.SIZE_LARGE){
            price+=hamburgerSizeLargePrice
        }
        if(this._staffing===Hamburger.STUFFING_CHEESE){
            price+=hamburgerStaffingCheesePrice
        }
        if(this._staffing===Hamburger.STUFFING_SALAD){
            price+=hamburgerStaffingSaladPrice
        }
        if(this._staffing===Hamburger.STUFFING_POTATO){
            price+=hamburgerStaffingPotatoPrice
        }
        if(this.toppings.some(function (item) {return item===Hamburger.TOPPING_MAYO})){
            price+=hamburgerToppingMayoPrice
        }
        if(this.toppings.some(function (item) {return item===Hamburger.TOPPING_SPICE})){
            price+=hamburgerToppingSpicePrice
        }
        return 'Price: '+price+' hrn'
    }

    calculateCalories() {
        let calories = 0,
            hamburgerSizeSmallCalories=20,
            hamburgerSizeLargeCalories=40,
            hamburgerStaffingCheeseCalories=20,
            hamburgerStaffingSaladCalories=5,
            hamburgerStaffingPotatoCalories=10,
            hamburgerToppingMayoCalories=5;

        if(this._size===Hamburger.SIZE_SMALL){
            calories+=hamburgerSizeSmallCalories
        }
        if(this._size===Hamburger.SIZE_LARGE){
            calories+=hamburgerSizeLargeCalories
        }
        if(this._staffing===Hamburger.STUFFING_CHEESE){
            calories+=hamburgerStaffingCheeseCalories
        }
        if(this._staffing===Hamburger.STUFFING_SALAD){
            calories+=hamburgerStaffingSaladCalories
        }
        if(this._staffing===Hamburger.STUFFING_POTATO){
            calories+=hamburgerStaffingPotatoCalories
        }
        if(this.toppings.some(function (item) {return item===Hamburger.TOPPING_MAYO})){
            calories+=hamburgerToppingMayoCalories
        }
        return 'Calories: '+calories
    }
}
Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';

function HamburgerException (message) {
    this.message = message
}
try{
    const hamburger= new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD );
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    console.log(hamburger.getTopping());

}catch (e) {
    alert(e.message)
}


