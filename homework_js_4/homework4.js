function createNewUser(name,secondName) {
    newUser = {
        firstName: name,
        lastName: secondName,
        login: function getLogin() {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
    };

    Object.defineProperty(newUser, 'firstName', {
        value: name,
        writable: false,
    });

    Object.defineProperty(newUser, 'lastName', {
        value: secondName,
        writable: false
    });

    console.log(newUser.login());

    return newUser;
}

createNewUser(prompt('What is your name?'),
    prompt ('What is your last name?'));

